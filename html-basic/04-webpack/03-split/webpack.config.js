const path = require('path');

module.exports = {
  mode: 'development',
  entry: {
    bundle: "./js/script.js",
    style: "./css/style.css"
  },
  output: {
    filename: "[name].js",
    path: path.resolve(__dirname, 'dist'),
  },
  module: {
    rules: [
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader'],
      },
    ],
  }
};

