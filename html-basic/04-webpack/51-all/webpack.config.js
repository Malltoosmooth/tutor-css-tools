const
  path = require('path'),
  HtmlWebpackPlugin = require('html-webpack-plugin'),
  MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
  mode: 'development',
  entry: {
    bundle: "./js/script.coffee",
    style: "./css/style.styl"
  },
  output: {
    filename: "[name].js",
    path: path.resolve(__dirname, 'dist'),
  },
  module: {
    rules: [
      {
        test: /\.pug$/i,
        use: [
          {
            loader: 'pug-loader',
            options: {
              pretty: true
            }
          }
        ],
      },
      {
        test: /\.styl$/i,
        use: [
          MiniCssExtractPlugin.loader, 
          'css-loader', 
          'stylus-loader'],
      },
      {
        test: /\.coffee$/,
        use: [ 'coffee-loader' ]
      }
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: "./pug/alert.pug"
    }),
    new MiniCssExtractPlugin({
      filename: "[name].css"
    })
  ],
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    compress: true,
    port: 9000
  }
};
