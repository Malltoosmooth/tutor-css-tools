const path = require('path');

module.exports = {
  mode: 'development',
  entry: {
    bundle: [
      "./js/script.js"
    ],
  },
  output: {
    filename: "[name].js",
    path: path.resolve(__dirname, 'dist'),
  }
};

