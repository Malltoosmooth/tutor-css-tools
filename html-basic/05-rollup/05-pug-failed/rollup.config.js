import css from 'rollup-plugin-css-only';
import copy from 'rollup-plugin-copy'
import pug from 'rollup-plugin-pug';

export default {
  input: 'src/entry.js',
  output: { 
    file: 'build/js/script.js',
    format: 'iife'
  },
  plugins: [
    css({ 
      output: 'build/css/style.css' 
    }),
    copy({
      targets: [
        { src: 'src/js/live.js', dest: 'build/js' },
      ]
    }),
    pug({
      //include: 'src/pug/**.pug',
      //pretty: true,
      //staticPattern: /\.static\.(?:pug|jade)$/
    })
  ]
}
