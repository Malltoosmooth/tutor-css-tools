import html from 'rollup-plugin-generate-html-template';
import css from 'rollup-plugin-css-only';
import coffeescript from 'rollup-plugin-coffee-script';

export default {
  input: 'src/entry.js',
  output: { 
    file: 'build/js/script.js',
    format: 'iife'
  },
  plugins: [
    coffeescript(),
    css({ 
      output: 'build/css/style.css' 
    }),
    html({
      template: 'src/html/alert.html',
      target: 'build/index.html',
      inject: 'head',
      externals: [
        { type: 'js', file: "js/script.js", pos: 'before' }
      ]
    })
  ]
}
