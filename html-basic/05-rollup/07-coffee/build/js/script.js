(function () {
  'use strict';

  document.addEventListener('DOMContentLoaded', function(event) {
    var alertButtons, i;
    alertButtons = document.getElementsByClassName('dismissable');
    i = 0;
    while (i < alertButtons.length) {
      alertButtons[i].onclick = function() {
        this.parentElement.style.display = 'none';
        console.log('Close Button. Element ' + this.parentElement.id + ' dismissed');
        return false;
      };
      i++;
    }
  });

}());
