export default {
  input: [ 
    'src/js/script.js',
    'src/css/style.css'
  ],
  output: { 
    file: 'build/js/script.js',
    format: 'iife'
  }
}
