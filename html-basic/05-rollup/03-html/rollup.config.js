import html from 'rollup-plugin-generate-html-template';
import css from 'rollup-plugin-css-only';

export default {
  input: 'src/entry.js',
  output: { 
    file: 'build/js/script.js',
    format: 'iife'
  },
  plugins: [
    css({ 
      output: 'build/css/style.css' 
    }),
    html({
      template: 'src/html/alert.html',
      target: 'build/index.html',
      inject: 'head',
      externals: [
        { type: 'js', file: "js/script.js", pos: 'before' }
      ]
    })
  ]
}
