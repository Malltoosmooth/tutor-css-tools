document.addEventListener 'DOMContentLoaded', (event) ->
  alertButtons = document.getElementsByClassName('dismissable')
  i = 0
  while i < alertButtons.length

    alertButtons[i].onclick = ->
      @parentElement.style.display = 'none'
      console.log 'Close Button. Element ' + @parentElement.id + ' dismissed'
      false

    i++
  return
