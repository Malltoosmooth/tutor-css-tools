const blueScale = [     '#E3F2FD',
  '#BBDEFB', '#90CAF9', '#64B5F6',
  '#42A5F5', '#2196F3', '#1E88E5',
  '#1976D2', '#1565C0', '#0D47A1'];

function getBezierParams(index) {
  const i = index + 1;

  // Initialize base points
  // with destructuring style
  let [mx, my] = [0, 190];
  let [c1x, c2x, c3x] = [-10, 70, 120];
  let c1y, c2y, c3y;
  let [s1x, s1y, s2x, s2y] = [170, 230, 200, 220];

  // Defining variant points
  my  -= 20*i;
  c1x += 10*i;
  [c1y, c2y, c3y] = [my-10, my-10, my+10]
  s1y -= 20*i;
  s2y -= 20*i;

  return [
    mx, my,
    c1x, c1y, c2x, c2y, c3x, c3y,
    s1x, s1y, s2x, s2y];
}

function getDPath(index) {
  const [
    mx, my,
    c1x, c1y, c2x, c2y, c3x, c3y,
    s1x, s1y, s2x, s2y
  ] = getBezierParams(index);

  return `M ${mx} ${my}`
      + ` C ${c1x} ${c1y} ${c2x} ${c2y} ${c3x} ${c3y}`
      + ` S ${s1x} ${s1y} ${s2x} ${s2y}`
      + ` L 200 0 L 0 0`
      + ` z`;
}

function getDPathVariant(index) {
  let [
    mx, my,
    c1x, c1y, c2x, c2y, c3x, c3y,
    s1x, s1y, s2x, s2y
  ] = getBezierParams(index+1);

  // shift
  c1x -= 20;
  c2x -= 20;
  c3x -= 20;
  s1x -= 20;

  // get flip baseline
  let [
    mx0, my0,
    c1x0, c1y0, c2x0, c2y0, c3x0, c3y0,
    s1x0, s1y0, s2x0, s2y0
  ] = getBezierParams(index);

  // flip
  c1y = 2*c1y + c1y0;
  c2y = 2*c2y + c2y0;
  c3y = 2*c2y + c3y0;

  // get the d string property
  return `M ${mx} ${my}`
      + ` C ${c1x} ${c1y} ${c2x} ${c2y} ${c3x} ${c3y}`
      + ` S ${s1x} ${s1y} ${s2x} ${s2y}`
      + ` L 200 0 L 0 0`
      + ` z`;
}

function getBezierStyle(index) {
  const bezier_fromto = getDPath(index);
  const bezier_middle = getDPathVariant(index);
  const duration = 5 + index*3/10;

  const styles_kf = `
    @keyframes area_${index} {
      from { d: path("${bezier_fromto}"); }
      50%  { d: path("${bezier_middle}"); }
      to   { d: path("${bezier_fromto}"); }
    }
`
  const styles_id = `
    path#svg_area_${index} {
      fill: ${blueScale[index]};
      opacity: 0.5;
      animation: area_${index} ${duration}s ease infinite normal;
    }
`
  return styles_kf + styles_id;
}

document.addEventListener(
  "DOMContentLoaded", function(event) {
    const range = [...Array(10)];
    let styles;

    range.forEach((_, index) => {
      styles += getBezierStyle(index);
    });

    // Create style document
    const css = document.createElement('style');
    css.type = 'text/css';
    css.appendChild(document.createTextNode(styles));
    document.getElementsByTagName("head")[0].appendChild(css);
});
