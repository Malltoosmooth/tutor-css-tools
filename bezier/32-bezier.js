document.addEventListener(
  "DOMContentLoaded", function(event) {
    const styles_kf_a5 = `
    @keyframes area_5 {
      from { d: path("M 0 70 C 50  60 70  60 120 80 S 170 110 200 100 L 200 0 L 0 0 z"); }
      50%  { d: path("M 0 90 C 30 100 50 100 100 80 S 150  50 200  60 L 200 0 L 0 0 z"); }
      to   { d: path("M 0 70 C 50  60 70  60 120 80 S 170 110 200 100 L 200 0 L 0 0 z"); }
    }
`;
    const styles_kf_l5 = `
    @keyframes line_5 {
      from { d: path("M 0 70 C 50 60 50 60 100 80 S 150 100 200 90"); }
      50%  { d: path("M 0 50 C 30 40 20 40 70 60 S 100 80 200 70"); }
      to   { d: path("M 0 70 C 50 60 50 60 100 80 S 150 100 200 90"); }
    }
`;
    const styles_id_a5 = `
    path#svg_area_5 {
      fill: #2196F3;
      animation: area_5 5s ease infinite normal;
    }
`;
    const styles_id_l5 = `
    path#svg_line_5 {
      animation: line_5 4s ease infinite normal;
    }
`;

    const styles = styles_kf_a5 + styles_kf_l5
                 + styles_id_a5 + styles_id_l5;

    // Create style document
    const css = document.createElement('style');
    css.type = 'text/css';
    css.appendChild(document.createTextNode(styles));
    document.getElementsByTagName("head")[0].appendChild(css);
});


