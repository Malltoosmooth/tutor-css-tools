module.exports = function(eleventyConfig) {

  // Directory Management
  eleventyConfig.addPassthroughCopy("assets");

  // Layout Alias
  eleventyConfig.addLayoutAlias("index", "layouts/index.njk");
  
  // Return your Config object
  return {
    // URL Related
    pathPrefix: "/",

    // Templating Engine
    templateFormats: [
      "njk",
      "html",
      "md"
    ],

    htmlTemplateEngine: "njk",

    // Directory Management
    passthroughFileCopy: true,
    dir: {
      input: "views",
      output: "dist",
      // ⚠️ These values are both relative to your input directory.
      includes: "_includes",
      data: "_data"
    }
  };
};
