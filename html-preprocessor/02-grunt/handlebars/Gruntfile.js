module.exports = function(grunt) {
  // configure the tasks
  let config = {

    'compile-handlebars': {
      home: {
        files: [ {
          cwd: "views",
          src: "*.hbs",
          dest: "build",
          expand: true,
          ext: ".html"
        } ],
        partials: 'views/partials/*.hbs'
      }
    },

    watch: {
      handlebars: {
        files: ['views/**/*'],
        tasks: ['compile-handlebars'],
        options: {
          livereload: true,
          interrupt: false,
          spawn: false
        }
      }
    }

  };

  grunt.initConfig(config);

  // load the tasks
  grunt.loadNpmTasks('grunt-compile-handlebars');
  grunt.loadNpmTasks('grunt-contrib-watch');

  // define the tasks
  grunt.registerTask('default', [
    'compile-handlebars', 'watch'
  ] );

};
