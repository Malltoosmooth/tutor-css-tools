document.addEventListener("DOMContentLoaded", function(event) { 

  w3_open(); // default behaviour.

  var openNav  = document.getElementById("openNav");
  var closeNav = document.getElementById("closeNav");

  openNav.onclick = function() {
    w3_open();
    return false;
  }
  closeNav.onclick = function() {
   w3_close();
   return false;
  }
});

function w3_open() {
  document.getElementById("main").style.marginLeft = "25%";
  document.getElementById("mySidebar").style.width = "25%";
  document.getElementById("mySidebar").style.display = "block";
  document.getElementById("openNav").style.display = 'none';
  console.log('Open Button Clicked');
}
function w3_close() {
  document.getElementById("main").style.marginLeft = "0%";
  document.getElementById("mySidebar").style.display = "none";
  document.getElementById("openNav").style.display = "inline-block";
  console.log('Close Button Clicked');
}
