module.exports = function(grunt) {
  // configure the tasks
  let config = {

    pug: {
      compile: {
        options: {
          data: {
            debug: false
          },
          pretty: true
        },
        files: [ {
          cwd: "views",
          src: "*.pug",
          dest: "build",
          expand: true,
          ext: ".html"
        } ]
      }
    },

    watch: {
      pug: {
        files: ['views/**/*'],
        tasks: ['pug'],
        options: {
          livereload: true,
          interrupt: false,
          spawn: false
        }
      }
    }

  };

  grunt.initConfig(config);

  // load the tasks
  grunt.loadNpmTasks('grunt-contrib-pug');
  grunt.loadNpmTasks('grunt-contrib-watch');

  // define the tasks
  grunt.registerTask('default', [
    'pug', 'watch'
  ] );

};
