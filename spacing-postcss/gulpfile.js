'use strict';

var gulp = require('gulp');

gulp.task('css', () => {
  const postcss    = require('gulp-postcss')
  const prettier   = require('gulp-prettier')
  const scss       = require('postcss-scss');

  return gulp.src('src/**/*.css')
    .pipe( postcss([ 
        require('postcss-strip-inline-comments'), 
        require('postcss-each'),
        require('precss')
      ], {syntax: scss}) 
    )
    .pipe( prettier() )
    .pipe( gulp.dest('dest/') )
})

gulp.task('default', gulp.series('css'));
