import { createWebHistory, createRouter } from "vue-router";
import HelloWorld from '@/components/HelloWorld'

const routes = [
  {
    path: '/',
    name: 'default',
    component: HelloWorld,
    meta: { title: 'Yellow' },
    props: { msg: 'How Are You?' }
  },
  {
    path: '/hello',
    name: 'Hello',
    component: HelloWorld,
    meta: { title: 'Hello' },
    props: { msg: 'Hello Earth!' }
  },
  {
    path: '/aloha',
    name: 'Aloha',
    component: HelloWorld,
    meta: { title: 'Aloha' },
    props: { msg: 'Aloha Maui!' }
  },
  {
    path: '/yellow',
    name: 'Yellow',
    component: HelloWorld,
    meta: { title: 'Yellow' },
    props: { msg: 'Yellow World!' }
  }
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
