const colors = require('tailwindcss/colors')

module.exports = {
  important: false,
  future: {
    purgeLayersByDefault: true,
  },
  purge: {
    enabled: true,
    content: [
      './public/**/*.html',
      './src/**/*.vue',
    ],
  },
  theme: {
    extend: {},
    colors: {
      black: '#000',
      white: '#fff',
      gray: colors.trueGray,
      red: colors.rose,
      teal: colors.teal,
      blue: colors.blue,
      orange: colors.orange
    }
  },
  variants: {},
  plugins: [],
}
