import Vue from 'vue'
import Router from 'vue-router'
import SimpleLayout from '@/components/SimpleLayout'
import SimpleInline from '@/components/SimpleInline'
import SimpleMockup from '@/components/mockup/SimpleMockup'
import SimpleTabs from   '@/components/simple/SimpleTabs'
import EnhancedTabs from '@/components/enhanced/EnhancedTabs'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'default',
      component: EnhancedTabs,
      meta: { title: 'Default Page' }
    },
    {
      path: '/simple-layout',
      name: 'SimpleLayout',
      component: SimpleLayout,
      meta: { title: 'Simple Tabs - Layout Only' }
    },
    {
      path: '/simple-inline',
      name: 'SimpleInline',
      component: SimpleInline,
      meta: { title: 'Simple Tabs - Inline' }
    },
    {
      path: '/simple-mockup',
      name: 'SimpleMockup',
      component: SimpleMockup,
      meta: { title: 'Simple Tabs - Mockup' }
    },
    {
      path: '/simple-tabs',
      name: 'SimpleTabs',
      component: SimpleTabs,
      meta: { title: 'Simple Tabs - Component' }
    },
    {
      path: '/enhanced-tabs',
      name: 'EnhancedTabs',
      component: EnhancedTabs,
      meta: { title: 'Enhanced Tabs - Component' }
    }
  ]
})
