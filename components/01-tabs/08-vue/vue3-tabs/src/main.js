import { createApp, reactive } from 'vue'
import App from './App.vue'
import router from './router'

const Title = reactive({
  computed: {
    pageTitle: function() {
      return this.$route.meta.title || 'Default Greet';
    }
  },
  created () {
    document.title = this.$route.meta.title;
  },
  watch: {
    $route(to) {
      document.title = to.meta.title;
    },
  }
})

createApp(App)
  .use(router)
  .mixin(Title)
  .mount('#app')
