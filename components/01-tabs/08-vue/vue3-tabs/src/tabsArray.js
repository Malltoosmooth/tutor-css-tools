let tabsArray = [
  { name: 'home', title: 'Home', color: 'bg-blue-500',
    text: `Lorem ipsum dolor sit amet,
      consectetur adipiscing elit.
      Quisque in faucibus magna.` },
  { name: 'team', title: 'Team', color: 'bg-teal-500',
    text: `Nulla luctus nisl in venenatis vestibulum.
      Nam consectetur blandit consectetur.` },
  { name: 'news', title: 'News', color: 'bg-red-500',
    text: `Phasellus vel tempus mauris,
      quis mattis leo.
      Mauris quis velit enim.` },
  { name: 'about', title: 'About', color: 'bg-orange-500',
    text: `Interdum et malesuada fames ac ante
      ipsum primis in faucibus.
      Nulla vulputate tortor turpis,
      at eleifend eros bibendum quis.` }
];

export { tabsArray };
