Vue.component('tabs', {
  template: `
  <main class="tabs">
    <div class="tab-headers">
      <div
         v-for="tab in tabs"
         v-on:click="selectTabByName(tab.name)"
         v-on:mouseenter="tab.isHovered = true"
         v-on:mouseleave="tab.isHovered = false"
         v-bind:class="[activeClass(tab), colorClass(tab)]">
        <div v-bind:class="{ 'bg-white' : tab.name == selected }"
          >{{ tab.title }}</div>
      </div>
    </div>

    <div class="tab-spacer"></div>

    <div class="tab-contents">
      <slot></slot>
    </div>
  </main>
  `,
  data() {
    return {
      selected: 'news',
      tabs: this.$children
    };
  },
  mounted() {
    this.selectTabByName(this.selected);
  },
  methods: {
    selectTabByName(tabName) {
      this.selected = tabName;
      this.tabs.forEach(tab => {
        tab.isActive = (tab.name == tabName);
      });
    },
    activeClass : function (tab) {
      return tab.name == this.selected ? 'active' : '';
    },
    colorClass  : function (tab) {
      return tab.name == this.selected ? tab.color : 'bg-gray-700';
    }
  }
});

Vue.component('tab', {
  template: `
     <div
       v-show="isActive"
       v-bind:class="this.color">
       <div class="tab-content"
            v-bind:class="{ 'is-hovered' : this.isHovered }">
         <slot></slot>
       </div>
     </div>
  `,
  props: {
    name:    { required: true },
    title:   { required: true },
    color:   { default: '' }
  },
  data() {
    return {
      isHovered: false,
      isActive: false
    };
  }
});
