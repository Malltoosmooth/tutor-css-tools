Vue.component('tabs', {
  template: `
  <main class="tabs">
    <div class="tab-headers">
      <div class="bg-gray-700"
           v-for="tab in tabs">{{ tab.title }}
      </div>
    </div>

    <div class="tab-spacer"></div>

    <div class="tab-contents">
      <slot></slot>
    </div>
  </main>
  `,

  data() {
    return {
      tabs: this.$children
    };
  }
});

Vue.component('tab', {
  template: `
     <div class="bg-gray-700" v-show="name == 'home'">
       <slot></slot>
     </div>
  `,
  props: {
    name:  { required: true },
    title: { required: true }
  }
});
