$(document).ready(function() {
  // Tab Headers: All Click Events
  $('.tab-headers > div').on('click', function (event) {
    const targetName = $(this)[0].dataset.target;
    const colorClass = $(this)[0].dataset.color;

    // Set all to default setting
    $('.tab-headers > div').each(function(el, i) {
      $(this)
        .removeClass('active')
        .removeClass(el.dataset.color)
        .addClass('bg-gray-700');
    });
    // Except the chosen one
    $(this)
      .addClass('active')
      .removeClass('bg-gray-700')
      .addClass(colorClass);

    // Showing the content
    $('.tab-contents > div').each(function(el, i) {
      el.style.display = 'none';
    });
    sel = $('#'+targetName);
    sel[0].style.display = 'block';
    sel.addClass(colorClass);
  });

  // Tab Headers: Trigger Default
  const tabHeaders  = $('.tab-headers').first();
  $('.active', tabHeaders)[0].click();
});
