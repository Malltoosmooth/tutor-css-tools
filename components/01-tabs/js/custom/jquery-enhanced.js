$(document).ready(function() {
  const tabHeaders  = $('.tab-headers').first();
  const tabContents = $('.tab-contents').first();

  // Tab Headers: All Click Events
  tabHeaders.on('click', 'div', function (event) {
    const targetName = $(this)[0].dataset.target;
    const colorClass = $(this)[0].dataset.color;

    // Set all to default setting
    tabHeaders.children('div').each(function(i, el) {
      $(this)
        .removeClass('active')
        .removeClass(el.dataset.color)
        .addClass('bg-gray-700')
        .children(":first").removeClass('bg-white');
    });
    // Except the chosen one
    $(this)
      .addClass('active')
      .removeClass('bg-gray-700')
      .addClass(colorClass)
      .children(":first").addClass('bg-white');

    // Showing the content
    tabContents.children('div').each(function() {
      $(this).hide();
    });
    $('#'+targetName)
      .show()
      .addClass(colorClass);
  });

  // Tab Headers: All Click Events
  tabHeaders.on('mouseenter', 'div', function (event) {
    const targetName = $(this)[0].dataset.target;
    $('#'+targetName).children(":first").addClass('is-hovered');
  });

  // Tab Headers: All Click Events
  tabHeaders.on('mouseleave', 'div', function (event) {
    const targetName = $(this)[0].dataset.target;
    $('#'+targetName).children(":first").removeClass('is-hovered');
  });

  // Tab Headers: Trigger Default
  $('.tab-headers .active').first().click();
});
