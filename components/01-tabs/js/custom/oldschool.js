var tabHeaders  = document.querySelector('.tab-headers');
var tabContents = document.querySelector('.tab-contents');

function activateTab(event) {
  var targetHeader = event.srcElement;
  var targetName = targetHeader.dataset.target;
  var colorClass = targetHeader.dataset.color;

  // Set all to default setting
  Array.from(tabHeaders.children)
    .forEach(function(tabHeader) {
      tabHeader.classList.remove("active");
      tabHeader.classList.remove(tabHeader.dataset.color);
      tabHeader.classList.add("bg-gray-700");
    });
  // Set the class state for the chosen one
  targetHeader.classList.add("active");
  targetHeader.classList.remove("bg-gray-700");
  targetHeader.classList.add(colorClass);
}

tabHeaders.querySelector('.active').click();
