function tabs() {
  return {
    tab: 'news',
    hover: false,
    header_home: {
      ['@click']()      { this.tab = 'home'; this.hover = this.tab; },
      ['@mouseenter']() { this.hover = (this.tab == 'home') ? this.tab : false; },
      ['@mouseleave']() { this.hover = false },
      [':class']()      { 
        return (this.tab == 'home') ? 'active bg-blue-500' : 'bg-gray-700';
      }
    },
    header_team: {
      ['@click']()      { this.tab = 'team'; this.hover = this.tab; },
      ['@mouseenter']() { this.hover = (this.tab == 'team') ? this.tab : false; },
      ['@mouseleave']() { this.hover = false },
      [':class']()      { 
        return (this.tab == 'team') ? 'active bg-teal-500' : 'bg-gray-700';
      }
    },
    header_news: {
      ['@click']()      { this.tab = 'news'; this.hover = this.tab; },
      ['@mouseenter']() { this.hover = (this.tab == 'news') ? this.tab : false; },
      ['@mouseleave']() { this.hover = false },
      [':class']()      { 
        return (this.tab == 'news') ? 'active bg-red-500' : 'bg-gray-700';
      }
    },
    header_about: {
      ['@click']()      { this.tab = 'about'; this.hover = this.tab; },
      ['@mouseenter']() { this.hover = (this.tab == 'about') ? this.tab : false; },
      ['@mouseleave']() { this.hover = false },
      [':class']()      { 
        return (this.tab == 'about') ? 'active bg-orange-500' : 'bg-gray-700';
      }
    },
    header_inner_home: {
      [':class']() { return (this.tab == 'home') ? 'bg-white' : ''; }
    },
    header_inner_team: {
      [':class']() { return (this.tab == 'team') ? 'bg-white' : ''; }
    },
    header_inner_news: {
      [':class']() { return (this.tab == 'news') ? 'bg-white' : ''; }
    },
    header_inner_about: {
      [':class']() { return (this.tab == 'about') ? 'bg-white' : ''; }
    },
    content_home: {
      ['x-show']() { return (this.tab == 'home'); },
      [':class']() { return (this.tab == 'home') ? 'bg-blue-500' : ''; }
    },
    content_team: {
      ['x-show']() { return (this.tab == 'team'); },
      [':class']() { return (this.tab == 'team') ? 'bg-teal-500' : ''; }
    },
    content_news: {
      ['x-show']() { return (this.tab == 'news'); },
      [':class']() { return (this.tab == 'news') ? 'bg-red-500' : ''; }
    },
    content_about: {
      ['x-show']() { return (this.tab == 'about'); },
      [':class']() { return (this.tab == 'about') ? 'bg-orange-500' : ''; }
    },
    content_inner_home: {
      [':class']() { return (this.hover == 'home') ? 'is-hovered' : ''; }
    },
    content_inner_team: {
      [':class']() { return (this.hover == 'team') ? 'is-hovered' : ''; }
    },
    content_inner_news: {
      [':class']() { return (this.hover == 'news') ? 'is-hovered' : ''; }
    },
    content_inner_about: {
      [':class']() { return (this.hover == 'about') ? 'is-hovered' : ''; }
    }
  }
}
