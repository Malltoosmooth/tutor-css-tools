function tabs() {
  return {
    tab: 'news',
    header_home: {
      ['@click']()      { this.tab = 'home' },
      [':class']()      { 
        return (this.tab == 'home') ? 'active bg-blue-500' : 'bg-gray-700';
      }
    },
    header_team: {
      ['@click']()      { this.tab = 'team' },
      [':class']()      { 
        return (this.tab == 'team') ? 'active bg-teal-500' : 'bg-gray-700';
      }
    },
    header_news: {
      ['@click']()      { this.tab = 'news' },
      [':class']()      { 
        return (this.tab == 'news') ? 'active bg-red-500' : 'bg-gray-700';
      }
    },
    header_about: {
      ['@click']()      { this.tab = 'about' },
      [':class']()      { 
        return (this.tab == 'about') ? 'active bg-orange-500' : 'bg-gray-700';
      }
    },
    content_home: {
      ['x-show']() { return (this.tab == 'home'); },
      [':class']() { return (this.tab == 'home') ? 'bg-blue-500' : ''; }
    },
    content_team: {
      ['x-show']() { return (this.tab == 'team'); },
      [':class']() { return (this.tab == 'team') ? 'bg-teal-500' : ''; }
    },
    content_news: {
      ['x-show']() { return (this.tab == 'news'); },
      [':class']() { return (this.tab == 'news') ? 'bg-red-500' : ''; }
    },
    content_about: {
      ['x-show']() { return (this.tab == 'about'); },
      [':class']() { return (this.tab == 'about') ? 'bg-orange-500' : ''; }
    },
  }
}
