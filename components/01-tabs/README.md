# Custom Tabs Component

## Credits

Inspired By

* [How To - Tab Headers][howto-01]

* [How To - Full Page Tabs][howto-02]

This component is a complete rewrite.

## Stylesheet Example

* Basic Layout using Flex,

* Styled Component,

* Tailwind CSS.

## Javascript Example

* Unobtrusive (Plain),

* jQuery,

* Alpine.

## Preview

Mobile First

![Tabs: Mobile][tabs-mobile]

Tablet

![Tabs: Desktop][tabs-tablet]

[howto-01]: https://www.w3schools.com/howto/howto_js_tab_header.asp
[howto-02]: https://www.w3schools.com/howto/howto_js_full_page_tabs.asp

[tabs-tablet]:  https://gitlab.com/epsi-rns/tutor-css-tools/-/raw/master/components/01-tabs/extra/tabs-mockup-tablet.png
[tabs-mobile]:  https://gitlab.com/epsi-rns/tutor-css-tools/-/raw/master/components/01-tabs/extra/tabs-mockup-mobile.png
